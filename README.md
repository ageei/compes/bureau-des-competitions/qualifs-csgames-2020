# Qualifs CSGames 2020

Ce dépôt contient les défis qui seront utilisés pour faire la sélection des membres des équipes de l'AGEEI pour les [CSGames 2020](http://2020.csgames.org/).

## Pour participer aux qualifications

Vous devrez remplir le [formulaire de participation](https://docs.google.com/forms/d/e/1FAIpQLSdrmJVem77JA90PlSFBpeU21prTdn4AfkiuDAWxkcbxbsupMg/viewform) et créez un dépôt Gitlab ou Github **privé** contenant vos solutions pour les défis.
Ce dépôt devra contenir un fichier `README` à sa racine dans lequel vous indiquerez votre nom et code permanent ainsi que la liste des défis pour lesquels vous présentez une solutions (partielle ou complète; une tentative de solution qui a du sens est mieux que rien du tout).

Vous devrez donner accès en tant que `Reporter` à votre dépôt à [@CycleOfTheAbsurd](https://gitlab.com/CycleOfTheAbsurd) et [@Calegh](https://gitlab.com/Calegh) afin que nous puissions évaluer vos soumissions.

### Organisation du dépôt

 - Vous devez créer un sous-dossier pour chaque catégorie d'épreuves (général, database, etc.). Utilisez les mêmes noms que dans le repo des qualifications (celui-ci). 
 - Si votre réponse pour une épreuve en particulier comporte plusieurs fichiers, créez un sous-dossier pour celle-ci avec un nom au format `epreuve_x` (par exemple, ça donnerait `gitlab.com/USER/REPO/general/epreuve_1`)
 - Si votre réponse ne comporte qu'un seul fichier, vous pouvez le placer directement dans le dossier de la catégorie (ex: `gitlab.com/USER/REPO/general/epreuve_1.xyz`).
 - Il est suggéré d'avoir un README dans chacun des dossiers pour expliquer vos solutions, le processus pour les déployer/exécuter, etc.

Le dépôt actuel contient un répertoire pour chaque catégorie de compétition et un `general` qui contient des défis informatiques généraux qui ne sont pas nécessairement associés à une compétition en particulier. Ceux-ci sont généralement plus accessibles que les défis particuliers à chaque compétition.
Dans chacun de ces répertoires se trouve un fichier `README`. Celui-ci contient les explications et les contraintes pour les défis de qualification. **Si vous ne respectez pas les contraintes données,** (format de remise, langages, technologies, etc.) **nous ne garantissons pas que votre soumission sera évaluée**. Ces contraintes sont en place pour assurer que nous puissions évaluer justement et dans un temps raisonnable toutes les solutions fournies.

Pour la plupart des épreuves, il vous sera demandé d'expliquer votre raisonnement/solution, ceci peut être relativement court (1 ou 2 paragraphes), mais n'est pas facultatif.

Pour certaines catégories (ex: Hardware), le `README` ne spécifiera pas de défi pour des raisons de temps et de faisabilité. Dans ces cas, nous vous invitons à nous faire part de vos expériences et connaissances liées au domaine ainsi qu'à mettre des liens vers des projets que vous avez fait, si applicable.

## Contraintes

À moins d'indications contraires, les programmes demandés doivent lire leurs entrées par **l'entrée standard (STDIN)** et afficher leurs réponses sur **la sortie standard (STDOUT)**.

## Liste des catégories

À noter, il est possible que certaines des catégories suivantes ne se retrouvent pas dans l'édition 2020 des CSGames, il s'agit d'une liste basée sur les compétitions annoncées et celles qui reviennent fréquemment. À l'inverse, il est aussi possible que certaines catégories qui seront aux CSGames cette année n'aient pas de défi de qualification associés.

 - [General](general/)
 - [Database](database/)
 - [Hardware](hardware/)
 - [Web](web/)
 - [Sécurité](securite/)
 - [Mobile](mobile/)
 - [Fonctionnel](fonctionnel/)
